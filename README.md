# Drupal Resources

A Drupal 8+ Resource List, So many good resources are scattered around the web I decided to make a list and share it so that others may suggest things to add and possible improvements.

# Drupal 8+ Resources List Free

# Other

System requirements – https://www.drupal.org/docs/system-requirements
Adding Arguments And Options To Deployer Tasks https://www.hashbangcode.com/article/adding-arguments-and-options-deployer-tasks

# Frontend (Free)

Create a Custom Drupal 8 Theme in 9 Simple Steps - https://www.specbee.com/blogs/how-create-custom-drupal-8-theme-9-simple-steps
How to Create a Favicon That Changes Automatically https://css-tricks.com/how-to-create-a-favicon-that-changes-automatically/

# CSS

CSS Animations - https://css-tricks.com/animating-with-lottie/
CSS Frosted glass credit card https://dev.to/dailydevtips1/css-frosted-glass-credit-card-3lak
Modern Sidebar Menu https://dev.to/florincornea/modern-sidebar-menu-53o1
Using CSS content-visibility to boost your rendering performance https://blog.logrocket.com/using-css-content-visibility-to-boost-your-rendering-performance/
Just CSS: CSS Like Button Animation https://dev.to/uzomezu/just-css-css-like-button-animation-2ll6
Modern CSS Solutions https://moderncss.dev/

# Backend (Free)

Create a block - https://www.specbee.com/blogs/programmatically-creating-block-in-drupal-8-brief-tutorial

# PHP / symfony (Free)

PHP Tutorials - https://www.w3schools.com/php/
Debugging PHP - https://www.specbee.com/blogs/php-debugging-how-debug-your-php-code-drupal-debugging-techniques-included
PHP Tutorials - https://phptherightway.com/
PHP Object Oriented Programming (OOPs) concept Tutorial with Example - https://www.guru99.com/object-oriented-programming.html
Creating a Twitch / IRC Chatbot in PHP with Minicli https://dev.to/erikaheidi/creating-a-twitch-irc-chatbot-in-php-with-minicli-45mo
symfony Console Commands https://symfony.com/doc/current/console.html
Modern PHP Cheat Sheet https://front-line-php.com/cheat-sheet
symfony 5 https://symfony.com/doc/current/the-fast-track/en/index.html

# Composer (Free)

Apply Drupal 9 compatibility patches - https://www.computerminds.co.uk/articles/apply-drupal-9-compatibility-patches-composer
Install a Node.js Project Within Composer - https://medium.com/myplanet-musings/how-to-install-a-node-js-project-within-composer-df9561a8cdb8

# tutorials Blog Style (Free)

Role Delegation Module - https://www.webwash.net/control-roles-assigned-in-drupal-using-role-delegation/
Test Driven Drupal presentation - https://www.oliverdavies.uk/blog/test-driven-drupal-presentation-drupalcon-europe
Behat (A comprehensive tutorial) - https://www.specbee.com/blogs/testing-your-drupal-website-just-got-easier-behat-comprehensive-tutorial
Converting a Module from Drupal 7 to Drupal 8 - https://www.unleashed-technologies.com/blog/tutorial-converting-module-drupal-7-drupal-8
Drupal 8: Writing a Hello World Module - https://drupalize.me/blog/201307/drupal-8-writing-hello-world-module
Drupal 8 Module Development, Parts 1,2,3: - https://getlevelten.com/blog/ian-whitcomb/drupal-8-module-development-part-1-getting-started https://getlevelten.com/blog/ian-whitcomb/drupal-8-module-development-part-2-forms https://getlevelten.com/blog/ian-whitcomb/drupal-8-module-development-part-3-plugins
Drupal 8 Module Development- http://mrf.github.io/drupal8modules/#/
Build a Drupal 8 Module: Routing, Controllers and Menu Links - https://www.sitepoint.com/build-drupal-8-module-routing-controllers-menu-links/
Building a Drupal 8 Module: Blocks and Forms - https://www.sitepoint.com/building-drupal-8-module-blocks-forms/
Building a Drupal 8 Module - Config and the Service Container - https://www.sitepoint.com/building-drupal-8-module-configuration-management-service-container/
The Drupal 8 version of EntityFieldQuery - https://www.sitepoint.com/drupal-8-version-entityfieldquery/
Drupal 8 Hooks and the Symfony Event Dispatcher - https://www.sitepoint.com/drupal-8-hooks-symfony-event-dispatcher/
Creating links with custom classes in drupal 8 modules - https://lohmeyer.rocks/blog/2015/12/03/0071-creating-links-custom-classes-drupal-8-modules
Building Drupal 8 modules: a practical guide - https://internetdevels.com/blog/building-drupal-8-modules-a-practical-guide
Build a Blog in Drupal 8: Content types and Fields - https://www.webwash.net/build-a-blog-in-drupal-8-content-types-and-fields/
Drupal 7 to Drupal 8 Migration 101 and Observations - http://www.tanay.co.in/blog/drupal-7-drupal-8-migration-101-and-observations.html
Drupal 8 Migration Checklist - https://www.drupalpartners.com/blog/drupal-8-migration-checklist
Drupal 8 Performance: Moving the service container cache away from the database - http://www.drupalonwindows.com/en/blog/drupal-8-performance-moving-service-container-cache-away-database
Update Your Nginx Config for Drupal 8 - https://pantheon.io/blog/update-your-nginx-config-drupal-8
A Simple Drupal 7 to Drupal 8 Migration - https://drupalize.me/blog/201511/simple-drupal-7-drupal-8-migration
Drupal 8 migration status and surprises - https://www.freelock.com/blog/john-locke/2016-01/drupal-8-migration-status-and-surprises
Migrate to Drupal 8 from a custom site - https://studio.gd/node/5
Backup Your Drupal 8 Database to S3 with Drush & Jenkins - https://chromatichq.com/blog/backup-your-drupal-8-database-s3-drush-jenkins
Your First Drupal 8 Migration - https://www.sitepoint.com/your-first-drupal-8-migration/
Drupal 6 to Drupal 8(.0.x) Custom Content Migration - https://www.drupaleasy.com/blogs/ultimike/2016/03/drupal-6-drupal-80x-custom-content-migration
Setup Drupal 8 RESTful Web Services in 3 Easy Steps - https://www.appnovation.com/blog/setup-drupal-8-restful-web-services-3-easy-steps
Using REST Export With Views In Drupal 8 - https://redcrackle.com/blog/rest-export-views-drupal-8
Drupal 8 REST Requests - https://swsblog.stanford.edu/blog/drupal-8-rest-requests
Creating a Multimedia Installation Using Drupal 8 - https://dev.acquia.com/blog/creating-a-multimedia-installation-using-drupal-8/18/02/2016/9641
APIs in Drupal 8: REST-aware Routing - https://dev.acquia.com/blog/apis-in-drupal-8/apis-in-drupal-8-restaware-routing/09/03/2016/9811
How to create Custom Rest Resources for POST methods in Drupal 8 - https://www.valuebound.com/resources/blog/how-to-create-custom-rest-resources-for-post-methods-drupal-8
Using Drupal 8 contact forms via REST - http://www.svendecabooter.be/blog/using-drupal-8-contact-forms-via-rest
8 Insights and Useful Snippets for the D8 Rest Module - https://www.mediacurrent.com/blog/8-insights-and-useful-snippets-d8-rest-module/
Drupal 8 multilingual tidbits 1: language first (See right hand panel for other parts) - http://hojtsy.hu/blog/2013-jun-11/drupal-8-multilingual-tidbits-1-language-first 
Writing custom fields in Drupal 8 - https://capgemini.github.io/drupal/writing-custom-fields-in-drupal-8/
How To Migrate WordPress To Drupal - https://www.cloudways.com/blog/migrate-wordpress-to-drupal/
Drupal 101: Installing Drupal 8 - https://chenhuijing.com/blog/drupal-101-getting-started-with-d8/
Our First Drupal 8 Site - https://www.drupalaid.com/blog/our-first-site-built-with-drupal-8-how-we-did-it
How to install Drupal 8 (and Composer) on Windows 10 Bash - https://polso.info/how-install-drupal-8-and-composer-windows-10-bash
6 steps for new Drupal 8 developers - https://www.webomelette.com/6-steps-new-drupal-8-developers
Drupal 8 Quick Tip: Entity View Hooks - https://www.alloymagnetic.com/blog/65/drupal-8-quick-tip-entity-view-hooks
Drupal 8 Custom Plugin Types - https://www.sitepoint.com/drupal-8-custom-plugin-types/
Adding Responsive Images to Your Drupal 8 Site - https://www.advomatic.com/insights/adding-responsive-images-to-your-drupal-8-site
How to Log Messages in Drupal 8 - https://drupalize.me/blog/201510/how-log-messages-drupal-8
Update your Nginx config to support Drupal 8 - https://insready.com/en/blog/update-your-nginx-config-support-drupal-8
Drupal 8 tip of the day: replace hook_drush_command() by a YAML file - https://blog.riff.org/2015_10_23_drupal_8_tip_of_the_day_replace_hook_drush_command_by_a_yaml_file
How to Help with the Drupal 8 Contrib Porting Tracker - https://www.hook42.com/blog/how-help-drupal-8-contrib-porting-tracker
Drupal 8 Headless Hello World - https://www.valuebound.com/resources/blog/drupal-8-headless-hello-world
Drupal 8 performance: enabling the classloader cache - http://www.drupalonwindows.com/en/blog/drupal-8-performance-enabling-classloader-cache
Creating the Drupal Cron Queue worker in Drupal 8 - https://vdmi.nl/blog/creating-drupal-cron-queue-worker-drupal-8
Configuration in Drupal 8: tips and examples for developers - https://internetdevels.com/blog/configuration-in-drupal-8
Clientside validation for Drupal 8 - https://attiks.com/blog/clientside-validation-for-drupal-8?pk_campaign=planet&pk_kwd=link
Where is hook_page_alter in Drupal 8? - https://cryptic.zone/blog/where-hook_page_alter-drupal-8
Drupal 8 + Gulp + BrowserSync - https://blog.merge.nl/20151123/drupal-8-gulp-browsersync
Drupal 8: Setting Up Multi-site - https://www.kalose.net/oss/drupal-8-setting-multi-site/
Drupal 8 Queue API - Powerful Manual and Cron Queueing - https://www.sitepoint.com/drupal-8-queue-api-powerful-manual-and-cron-queueing/
Build Edge-to-edge Sites using Paragraphs Module in Drupal 8 - https://www.webwash.net/build-edge-to-edge-sites-using-paragraphs-module-in-drupal-8/
How to Create Powerful Container Paragraphs in Drupal 8 - https://www.webwash.net/how-to-create-powerful-container-paragraphs-in-drupal-8/
Drupal 8 Module of the Week: BigPipe - https://dev.acquia.com/blog/drupal-8-module-of-the-week/drupal-8-module-of-the-week-bigpipe/22/01/2016/9566
Drupal 8, hook_form_alter() - https://jordanpagewhite.github.io/drupal-8-hook-form-alter/
Build a custom calendar module in Drupal 8 - https://arrea-systems.com/custom_calendar_module_Drupal_8
Unloading jQuery in Drupal 8 - https://chromatichq.com/blog/unloading-jquery-drupal-8
How to Build Multi-step Forms in Drupal 8 - https://www.sitepoint.com/how-to-build-multi-step-forms-in-drupal-8/
Adding new HTML tags in the <head> in Drupal 8 - https://www.webomelette.com/adding-new-html-tags-drupal-8
Drupal 8 Guided Tour module - https://arrea-systems.com/Drupal_8_Guided_Tour_module
Drupal 8 Module of the Week: Coder - https://dev.acquia.com/blog/drupal-8-module-of-the-week/drupal-8-module-of-the-week-admin-toolbar/04/02/2016/9661
Manage your articles using Taxonomy in Drupal 8 - https://www.valuebound.com/resources/blog/manage-your-article-using-taxonomy-in-drupal-8
Install and use Swift Mailer in Drupal 8 - https://arrea-systems.com/Install_use_SwiftMailer_Drupal_8_(part_1_dependency)
Drupal 8 Cheatsheet for Developers - https://cryptic.zone/blog/drupal-8-cheatsheet-developers
Drupal 8 SEO: Differences between simple_sitemap and xmlsitemap - https://gbyte.dev/blog/drupal8-seo-simple_sitemap-vs-xmlsitemap-differences
Image effects module for Drupal 8 - http://janezurevc.name/image-effects-module-for-drupal-8
Creating modal windows (pop-ups) in Drupal 8: full tutorial - https://internetdevels.com/blog/creating-popups-in-Drupal-8
Drupal 8 development: useful tips for devs - https://internetdevels.com/blog/drupal-8-development-useful-tips
Enabling "development mode" on a local Drupal 8 site - https://www.drupaleasy.com/quicktips/enabling-development-mode-local-drupal-8-site
Disable Drupal 8 cache during development - https://knackforge.com/blog/rajamohamed/disable-drupal-8-cache-during-development
Drupal 8 Module of the Week: Linkit - https://dev.acquia.com/blog/drupal-8-module-of-the-week/drupal-8-module-of-the-week-linkit/22/03/2016/9901
Use Drupal 8 Cache Tags with Varnish and Purge - https://www.jeffgeerling.com/blog/2016/use-drupal-8-cache-tags-varnish-and-purge
Drupal 8 Module of the Week: Search API - https://dev.acquia.com/blog/drupal-8-module-of-the-week/drupal-8-module-of-the-week-search-api/29/03/2016/10201
Overriding Drupal 8 Configuration Values in settings.php - https://www.chapterthree.com/blog/overriding-drupal-8-configuration-values-settingsphp
How to write the custom Drush Commands in Drupal 8? - https://www.valuebound.com/resources/blog/how-to-write-custom-drush-commands-drupal-8
Injecting services in your D8 plugins  - https://www.lullabot.com/articles/injecting-services-in-your-d8-plugins
Drupal 8 performance: the Supercache module - http://www.drupalonwindows.com/en/blog/drupal-8-performance-supercache-module
Taking a (Drupal 8) website offline using AppCache - https://realize.be/blog/taking-drupal-8-website-offline-using-appcache
Demystifying Drupal 8's breakpoints.yml file - https://www.drupaleasy.com/quicktips/demystifying-drupal-8s-breakpointsyml-file?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+DrupalEasy+%28DrupalEasy%29
Sharing Breakpoints Between Drupal 8 and Sass  - https://www.lullabot.com/articles/sharing-breakpoints-between-drupal-8-and-sass
Drupal 8 Module of the Week: Monolog - https://dev.acquia.com/blog/drupal-8-module-of-the-week/drupal-8-module-of-the-week-monolog/19/04/2016/10311
Drupal 8 : Inject a contact form inside a content type in 5 steps - https://www.flocondetoile.fr/blog/drupal-8-inject-contact-form-inside-content-type-5-steps
Porting token module to Drupal 8 - http://hussainweb.me/porting-token-module-drupal-8/
Introducing an HTTP API for Drupal 8 Entity Queries - https://atendesigngroup.com/articles/introducing-http-api-drupal-8-entity-queries
Case Study: Launching Inventory Control System with Commerce 8.x-2.0-alpha4 on Drupal 8.1 - https://insready.com/en/blog/case-study-launching-inventory-control-system-commerce-8x-20-alpha4-drupal-81
Just in case - Drupal 8's /core/rebuild.php - https://www.drupaleasy.com/quicktips/just-case-drupal-8s-corerebuildphp
Saving and retrieving taxonomy terms programmatically for Drupal 8 - https://www.btmash.com/article/2016-04-26/saving-and-retrieving-taxonomy-terms-programmatically-drupal-8/
How to Create a Search Page in Drupal 8 - https://www.webwash.net/how-to-create-a-search-page-in-drupal-8/
Decoupled Blocks with Drupal 8 and JavaScript - https://www.mediacurrent.com/blog/decoupled-blocks-drupal-8-and-javascript/
Brightcove Video Connect for Drupal 8 - Part 1: Architecture summary and Technical approach for development - https://pronovix.com/blog/brightcove-video-connect-drupal-8-part-1-architecture-summary-and-technical-approach
4 New CKEditor Plugins For Drupal 8 - https://www.darryl.ai/blog/4-new-ckeditor-plugins-for-drupal-8/
Speed up Drupal 8 block rendering with Blackfire.io - https://evolvingweb.ca/blog/speed-drupal-8-block-rendering-blackfireio
Adding a role to a user programmatically in Drupal 8 - https://www.jeffgeerling.com/blog/2016/adding-role-user-programmatically-drupal-8
How to Register an Event Subscriber in Drupal 8 - https://www.chapterthree.com/blog/how-to-register-event-subscriber-drupal8
Applying BEM inspired classes to your Drupal 8 theme - https://thejibe.com/articles/applying-bem-inspired-classes-to-your-drupal-8-theme
Drupal 8 Module of the Week: Media Entity - https://dev.acquia.com/blog/drupal-8-module-of-the-week/drupal-8-module-of-the-week-media-entity/31/05/2016/10611
Build robust forms in Drupal 8 - https://newmediadenver.com/build-robust-forms-in-drupal-8/
Drupal 8 Debugging: Kareful Klicking in Kint - https://www.drupaleasy.com/quicktips/drupal-8-debugging-kareful-klicking-kint
How to enhance your content authoring by adding custom CKEditor plugin in Drupal 8? - https://www.valuebound.com/resources/blog/how-to-enhance-your-content-authoring-by-adding-custom-ckeditor-plugin-drupal-8
How to write a PHPUnit functional test for Drupal 8 - https://www.computerminds.co.uk/drupal-code/how-write-phpunit-functional-test-drupal-8
Debug Site Performance Using Web Profiler in Drupal 8 - https://www.webwash.net/debug-site-performance-using-web-profiler-in-drupal-8/
Let’s debug in Drupal 8 ! - https://www.liip.ch/en/blog/lets-debug-drupal-8
Digging Into Drupal 8: Code Snippets for Site Builders - https://chromatichq.com/blog/digging-drupal-8-code-snippets-site-builders
Drupal 8 Module of the Week: Permissions by Term - https://dev.acquia.com/blog/drupal-8-module-of-the-week/drupal-8-module-of-the-week-permissions-by-term/18/07/2016/16251
The BigPipe Drupal 8 module for your website performance optimization - https://internetdevels.com/blog/bigpipe-drupal8-module-for-website-performance
How to create Queue worker in Drupal 8 https://knackforge.com/blog/vamsi/how-create-queue-woker-drupal-8
Drupal 8: overview of the Commerce 2.x module for online stores https://internetdevels.com/blog/the-commerce-2x-module-for-Drupal-8
Batch Process in Drupal 8 https://www.valuebound.com/resources/blog/batch-process-drupal-8
Drupal 8 Namespaces - class aliasing https://www.computerminds.co.uk/drupal-code/drupal-8-namespaces-class-aliasing
How to Create Custom field in Drupal 8 (Part 1) https://www.valuebound.com/resources/blog/how-to-create-custom-field-drupal-8-part-1
How to send a mail programmatically in Drupal 8 https://www.zyxware.com/articles/5504/drupal-8-how-to-send-a-mail-programmatically-in-drupal-8
Create a Custom Views Sort Plugin with Drupal 8 https://chromatichq.com/blog/create-custom-views-sort-plugin-drupal-8
Styling Views Exposed Filters Selects in Drupal 8 http://jimbir.ch/blog/styling-views-exposed-filters-selects-drupal-8
Views in Drupal 8: Improved Displays https://www.advomatic.com/insights/views-in-drupal-8-improved-displays
Your First RESTful View in Drupal 8 https://drupalize.me/blog/201402/your-first-restful-view-drupal-8
Creating a custom Views field in Drupal 8 https://www.webomelette.com/creating-custom-views-field-drupal-8
Creating a custom Views filter in Drupal 8 https://www.webomelette.com/creating-custom-views-filter-drupal-8
Drupal 8: Programmatically Adding a Views Footer https://atendesigngroup.com/articles/drupal-8-programmatically-adding-views-footer
How to theme Drupal 8 views by overriding default templates https://redcrackle.com/blog/drupal-8/theme-views-templates
How to attach a CSS or JS library to a View in Drupal 8 https://www.jeffgeerling.com/blog/2020/how-attach-css-or-js-library-view-drupal-8
Creating a Drupal 8 Theme with Sass,Singularity & Breakpoint https://www.appnovation.com/blog/creating-drupal-8-theme-sasssingularity-breakpoint
Up and Theming with Drupal 8 https://thinkshout.com/blog/2015/11/up-and-theming-with-drupal-8/
Adding Google Fonts to a Drupal 8 theme https://www.kinetasystems.com/blog/adding-google-fonts-to-a-drupal-8-theme
Using Twig in Drupal 8 https://internetdevels.com/blog/using-twig-in-drupal-8
Creating Links Within Twig Templates Using path() and url() https://chromatichq.com/blog/creating-links-within-twig-templates-using-path-and-url
Theming in Drupal 8: tips and examples for developers https://internetdevels.com/blog/theming-in-drupal-8-tips-and-examples
New Tools for Drupal 8 Theming: Defining and Using Libraries https://evolvingweb.ca/blog/new-tools-drupal-8-theming-defining-and-using-libraries-0
Drupal 8 Theming Best Practices https://www.gregboggs.com/drupal-8-theming-best-practices/
Including SVG icons in a Drupal 8 theme https://red-route.org/articles/including-svg-icons-drupal-8-theme
Drupal 8: How to Craft Custom Theme Hook Suggestions & Templates https://www.dannyenglander.com/blog/drupal-8-theming-tutorial-how-craft-custom-theme-hook-suggestions-and-templates/
Creating Custom Field Formatters in Drupal 8 https://www.sitepoint.com/creating-custom-field-formatters-drupal-8/
Drupal 8 Field API series part 1: field formatters https://realize.be/blog/drupal-8-field-api-series-part-1-field-formatters
Creating a custom block in Drupal 8 https://openahmed.com/blog/creating-custom-block-drupal-8
Create a block programatically in Drupal 8 http://dribbit.eu/drupal-8/create-a-block-in-drupal-8
Adding CSS Classes to Blocks in Drupal 8 https://atendesigngroup.com/articles/adding-css-classes-blocks-drupal-8
Custom Block in Drupal 8 https://arrea-systems.com/node/26
Drupal 8: Create a Simple Module https://www.kalose.net/oss/drupal-8-create-simple-module/
Say Hello world to Drupal 8! [Basic steps involved in Creating a custom module in Drupal 8] https://redcrackle.com/blog/say-hello-world-drupal-8-basic-steps-involved-creating-custom-module-drupal-8
Writing Simple (PHPUnit) Tests for Your D8 module https://www.mediacurrent.com/blog/writing-simple-phpunit-tests-your-d8-module/
PHPUnit in Drupal https://www.drupal.org/docs/automated-testing/phpunit-in-drupal
How to import excel data to Drupal with custom module https://www.digitalnadeem.com/2020/02/01/how-to-import-excel-data-to-drupal-with-custom-module/
Adventures in Drupal 8 Module Development https://docs.google.com/presentation/d/1_cDZBhabYup9YEDoyoHiV0TIVnJ1JIlVBWsj9eiOYT4/edit#slide=id.p
How to build a Drupal Commerce development environment https://www.centarro.io/blog/how-build-drupal-commerce-development-environment
GraphQL with Drupal 8 : The All-You-Need-To-Know Guide (with examples!) https://www.specbee.com/blogs/graphQL-with-drupal-8-what-is-graphql-Advantages-need-to-know-Guide
A Look At Drupal Content Deployment https://www.codeenigma.com/drupal-content-deployment?language_content_entity=en
A personal reader for Drupal https://realize.be/blog/personal-reader-drupal
Drupal 8 Custom Modules – Creating efficient Drupal 8 modules with these best practices https://www.specbee.com/blogs/drupal-8-custom-modules-creating-efficient-drupal-8-modules-with-these-best-practices
How to redirect a user after login in Drupal the proper way https://www.thesavvyfew.com/insights/how-redirect-user-after-login-drupal-proper-way

Using the Configuration Module Filter in Drush 8 https://pantheon.io/blog/using-configuration-module-filter-drush-8
Per Environment Config in Drupal 8 https://www.davehall.com.au/blog/dave/2016/01/25/environment-config-drupal-8
Why it’s worth building a membership website in Drupal 8 https://drudesk.com/blog/building-membership-in-drupal-8
Building a Membership site in Drupal 8 https://www.freelock.com/blog/john-locke/2017-07/building-membership-site-drupal-8
Are you using the Drupal Spec Tool? Because if you aren’t, you should be. https://mikemadison.net/blog/2020/8/31/introduction-to-the-drupal-spec-tool

# Drupal 9

Automate Drupal 9 upgrades: Addressing deprecated code as a community  https://drupal.tv/external-video/2020-03-19/automate-drupal-9-upgrades-addressing-deprecated-code-community
Demystifying Composer https://drupal.tv/index.php/external-video/2019-02-03/demystifying-composer
Preparing your site for Composer 2 https://www.drupal.org/docs/develop/using-composer/preparing-your-site-for-composer-2
My PHPUnit configuration for my Drupal projects https://mglaman.dev/blog/my-phpunit-configuration-my-drupal-projects
Screencast: Updating to Drupal 9 in 15 minutes https://matthewgrasmick.com/posts/screencast-updating-drupal-9-15-minutes
Drupal 9: Auto Injecting Paragraph Forms On Node Edit Pages https://www.hashbangcode.com/article/drupal-9-auto-injecting-paragraph-forms-node-edit-pages
Business Process Automation with Drupal https://www.specbee.com/blogs/business-process-automation-with-drupal-top-drupal-8-and-9-modules-and-distributions
Improving Drupal 9 Performance with modules, best coding practices and the right server configuration https://www.specbee.com/blogs/improving-drupal-9-performance-modules-best-coding-practices-and-right-server-configuration

# Decoupled Drupal 

Decoupled Drupal: Getting Started with Gatsby and JSON:API https://www.lullabot.com/articles/decoupled-drupal-getting-started-gatsby-and-jsonapi

# IDEs

PHPStorm:
PHPstorm for D8 - https://redcrackle.com/blog/drupal-8/phpstorm 

VSCode:
Speed up your development with this new VS Code extension https://dev.to/alex_barashkov/speed-up-your-development-with-this-new-vs-code-extension-5b3m
224: VS Code Tasks https://css-tricks.com/newsletter/224-vs-code-tasks/

# tutorials Video Style (Free)

# DDEV

DDEV extented https://www.slideshare.net/cmuench/ddev-extended
Creating a custom PHPUnit command for DDEV https://www.oliverdavies.uk/blog/creating-custom-phpunit-command-ddev
Run Drush commands on DDEV without ”exec” https://thamas.hu/drupal-drush-commands-on-ddev-without-exec/
How can I use xdebug 3.0 with PHP 7.3 or 7.4 in DDEV? https://stackoverflow.com/questions/65137275/how-can-i-use-xdebug-3-0-with-php-7-3-or-7-4-in-ddev/65137276#65137276

# Git

The Git Rerere Command — Automate Solutions to Fix Merge Conflicts https://levelup.gitconnected.com/the-git-rerere-command-automate-solutions-to-fix-merge-conflicts-d501a9ab9007
Make advanced Git tasks simple with Lazygit https://opensource.com/article/20/3/lazygit

# Bash
My favorite Bash hacks https://opensource.com/article/20/1/bash-scripts-aliases

#Tools

Backup Support for MariaDB-Exclusive Features https://mariadb.com/kb/en/mariabackup-overview/
A Git Cheatsheet Of Commands Every Developer Should Use  https://dev.to/ravimengar/a-git-cheatsheet-of-commands-every-developer-should-use-38ma
5 Git Commands That Don’t Get the Hype They Should https://towardsdatascience.com/5-git-commands-that-dont-get-the-hype-they-should-d62af563acaa
7 Git tricks that changed my life https://opensource.com/article/20/10/advanced-git-tips
Easily manage git hooks in your composer config https://packagist.org/packages/brainmaestro/composer-git-hooks
Drupal Install Profile Tools - https://github.com/jenitehan/drupal_ip_tools
UpTime monitor tool https://uptimerobot.com/
Certbot https://certbot.eff.org/lets-encrypt/arch-apache
Build your own URL shortener in 15 minutes  https://dev.to/commonshost/build-your-own-url-shortener-in-15-minutes-279n
How to Create Simple Shell Scripts in Linux https://www.tecmint.com/create-shell-scripts-in-linux/
voice software https://talonvoice.com/
Simple and privacy-friendly alternative to Google Analytics https://plausible.io/
Command Line Interface Guidelines https://clig.dev/#foreword
Best Practices for Writing a Dockerfile https://blog.bitsrc.io/best-practices-for-writing-a-dockerfile-68893706c3
sitespeed tester https://www.sitespeed.io/
Converting and Optimizing Images From the Command Line (Ubuntu based) https://css-tricks.com/converting-and-optimizing-images-from-the-command-line/
Profiling PHP in production at scale https://calendar.perfplanet.com/2020/profiling-php-in-production-at-scale/
Jenis tools https://github.com/jenitehan
Chris remote dev setup https://github.com/ctorgalson/remote_dev
How to install glances on Arch Linux https://snapcraft.io/install/glances/arch
Free PNG site https://2.flexiple.com/scale/all-illustrations
Depix is a tool for recovering passwords from pixelized screenshots. https://github.com/beurtschipper/Depix
Handful of useful resources for Linux command line and bash shell scripting https://github.com/learnbyexample/scripting_course/blob/master/Linux_curated_resources.md
URL checker https://urlscan.io/
Site speed checker with detailed breakdown https://yellowlab.tools/
# CI/CD Pipeline

The Next 11 Things You Should Do For CI/CD Pipeline Optimization https://hackernoon.com/the-next-11-things-you-should-do-for-cicd-pipeline-optimization-tb1a3wse
Building a simple CI/CD pipeline for local testing using Go, Docker, Minikube and a Bash script https://dev.to/cishiv/building-a-simple-ci-cd-pipeline-for-local-testing-using-go-docker-minikube-and-a-bash-script-1647
How to Deploy a Container Using Ansible https://thenewstack.io/how-to-deploy-a-container-using-ansible/
Web Development from scratch: CI/CD Infrastructure & Development Process https://dev.to/hostman_com/web-development-from-scratch-ci-cd-infrastructure-development-process-3hpl
Craft a complete GitLab pipeline for Angular. Part 1 https://indepth.dev/posts/1374/craft-a-complete-angular-gitlab-pipeline
Building a Simple CLI Tool with Golang https://blog.rapid7.com/2016/08/04/build-a-simple-cli-tool-with-golang/amp/
How to use GitLab for Agile, CI/CD, GitOps, and more https://about.gitlab.com/blog/2020/12/17/gitlab-for-cicd-agile-gitops-cloudnative/
The basics of CI: How to run jobs sequentially, in parallel, or out of order https://about.gitlab.com/blog/2020/12/10/basics-of-gitlab-ci-updated/
Using Your CI/CD Pipeline To Prevent Your App From Getting Hacked https://dev.to/flippedcoding/using-your-ci-cd-pipeline-to-prevent-your-app-from-getting-hacked-1pk7
An Installer for Drupal 8 and GitLab CI  https://www.lullabot.com/articles/installer-drupal-8-and-gitlab-ci

# Automated Testing 

Drupal automated testing workshop https://github.com/opdavies/workshop-drupal-automated-testing
Testing Your Drupal Website just got easier with Behat (A comprehensive tutorial) https://www.specbee.com/blogs/testing-your-drupal-website-just-got-easier-behat-comprehensive-tutorial

# Sites

https://www.cocomore.com/blog/category/7
http://befused.com/drupal
https://virtuoso-performance.com/
https://www.ostraining.com/blog/drupal/
https://buildamodule.com/
https://blog.riff.org/tags/drupal8
https://lohmeyer.rocks/blog-categories/drupal
https://mateuaguilo.com/
https://vulcanr.me/

# Hosting

http://www.aegirproject.org/ https://docs.aegirproject.org/
https://docs.platform.sh/overview/pricing/sponsored.html

# Drupal 8+ Resources List Paid

# Frontend (Paid)

# Backend (Paid)

# PHP (Paid)

# Module tutorials (Paid)
